import 'package:faqcovid19/Pages/PDFGenerator.dart';
import 'package:faqcovid19/Pages/Questions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Gestes.dart';
import 'Symptomes.dart';
import 'Formulaire.dart';

class Homepage extends StatefulWidget {

  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {

  @override
  Widget build(BuildContext context) {
    //Construction du fond d'écran
    final background = Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
    );

//Retour visuel de tous les widgets de la page.
    return Scaffold(
        appBar: AppBar(title: Text('Tout savoir sur le COVID 19'),
          backgroundColor: Colors.blue,
          centerTitle: true,
        ),
        body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              background,
              Center(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 50.0),

                      child: MaterialButton(
                          height: 40.0,
                          minWidth: 1000.0,
                          color: Colors.cyan,
                          textColor: Colors.white,
                          disabledColor: Colors.cyan,
                          disabledTextColor: Colors.white,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.blueAccent,
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Gestes()));
                        },

                          child: Text('Les gestes barrières'),
                  )
              ),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 50.0),

                          child: MaterialButton(
                            height: 40.0,
                            minWidth: 1000.0,
                            color: Colors.cyan,
                            textColor: Colors.white,
                            disabledColor: Colors.cyan,
                            disabledTextColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.blueAccent,
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => Symptomes()));
                            },

                            child: Text('Les symptômes'),
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 50.0),

                          child: MaterialButton(
                            height: 40.0,
                            minWidth: 1000.0,
                            color: Colors.cyan,
                            textColor: Colors.white,
                            disabledColor: Colors.cyan,
                            disabledTextColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.blueAccent,
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => PDF()));
                            },

                            child: Text('Attestation'),
                          )
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 50.0),

                          child: MaterialButton(
                            height: 40.0,
                            minWidth: 1000.0,
                            color: Colors.cyan,
                            textColor: Colors.white,
                            disabledColor: Colors.cyan,
                            disabledTextColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.blueAccent,
                            onPressed: () {
                              Navigator.of(context).pop();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => QuestionsPage()));
                            },
                            child: Text('Questions'),
                          )
                      ),

                      Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 50.0),

                          child: MaterialButton(
                            height: 40.0,
                            minWidth: 1000.0,
                            color: Colors.cyan,
                            textColor: Colors.white,
                            disabledColor: Colors.cyan,
                            disabledTextColor: Colors.white,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.blueAccent,

                            child: Text('Envoyez-nous votre question ici'),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Formulaire()),
                              );
                            },
                          )
                      ),
              ]
    )
              )
            ]
        )
    );
  }
}
