import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';

import 'Gestes.dart';
import 'Homepage.dart';
import 'PDFGenerator.dart';
import 'Questions.dart';
import 'Symptomes.dart';

// Define a custom Form widget.
class Formulaire extends StatefulWidget {
  @override
  FormulaireState createState() {
    return FormulaireState();
  }
}


class FormulaireState extends State<Formulaire> {

  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  final _recipientController = TextEditingController(
      text: 'florence.meziere.simplon@gmail.com');
  final _subjectController = TextEditingController(text: '');

  final _bodyController = TextEditingController(
    text: '',
  );



  @override
  Widget build(BuildContext context) {
    final background = Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
    );
    // Construire le formulaire avec _formKey créé au-dessus
    return Scaffold(
        appBar: AppBar(title: Text('Tout savoir sur le COVID 19'),
          backgroundColor: Colors.blue,
          centerTitle: true,
        ),

        body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              background,
            Center(
              child: Container(
                width: 450.0,
                height: 460.0,

                child: Form(
                key: _formKey,
                  child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 50.0),
                      child: new Column(
                        children: <Widget>[
                          new TextFormField(
                            controller: _subjectController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Merci d\'entrer votre mail';
                              }
                              return null;
                            },
                            keyboardType:
                            TextInputType.emailAddress, // Use email input type for emails.
                            decoration: new InputDecoration(
                                hintText: '', labelText: 'Adresse email'),
                          ),
                          new TextFormField(
                            controller: _bodyController,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Merci d\'entrer votre message';
                              }
                              return null;
                            },
                            decoration: new InputDecoration(
                                hintText: '', labelText: 'Votre question'),
                          ),
                          new Container(
                            child: new MaterialButton(
                              height: 40.0,
                              minWidth: 1000.0,
                              color: Colors.cyan,
                              textColor: Colors.white,
                              disabledColor: Colors.cyan,
                              disabledTextColor: Colors.white,
                              padding: EdgeInsets.all(8.0),
                              splashColor: Colors.blueAccent,

                              onPressed: send,
                              child: new Text('Envoyer'),

                            ),
                            margin: new EdgeInsets.only(top: 20.0),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              )
              )
            )
            ]
        ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            ListTile(
              title: Text('Accueil',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Homepage()));
              },
            ),

            ListTile(
              title: Text('Les gestes barrières',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gestes()));
              },
            ),
            ListTile(
              title: Text('Les symptômes',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Symptomes()));
              },

            ),
            ListTile(
              title: Text('Attestation',
                style: TextStyle(fontSize: 20.0),
              ),

              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PDF()));
              },
            ),
            ListTile(
              title: Text('Questions',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionsPage()));
              },

            ),
            ListTile(
              title: Text('Posez nous vos question',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Formulaire()));
              },

            ),
          ],
        ),
      ),
    );
  }

  Future<void> send() async {
    final Email email = Email(
      body: _bodyController.text,
      subject: _subjectController.text,
      recipients: [_recipientController.text],

    );

    String platformResponse;

    try {
      await FlutterEmailSender.send(email);
      platformResponse = 'success';
    } catch (error) {
      platformResponse = error.toString();
    }

    if (!mounted) return;

    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(platformResponse),
    ));
  }
}

