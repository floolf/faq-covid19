import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';
import 'package:flutter_html_to_pdf/flutter_html_to_pdf.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

import 'Formulaire.dart';
import 'Gestes.dart';
import 'Homepage.dart';
import 'Questions.dart';
import 'Symptomes.dart';



class PDF extends StatefulWidget {
  @override
  _PDFState createState() => _PDFState();
}

class _PDFState extends State<PDF> {
  String generatedPdfFilePath;

  final _formKey = GlobalKey<FormState>();

  final format = DateFormat("yyyy-MM-dd");
  final format2 = DateFormat("HH:mm");

  bool _value1 = false;
  bool _value2 = false;
  bool _value3 = false;
  bool _value4 = false;
  bool _value5 = false;
  bool _value6 = false;
  bool _value7 = false;

  String _nom, _prenom;

  @override
  void initState() {
    super.initState();
    generateExampleDocument();
  }


  Future<void> generateExampleDocument() async {
    var htmlContent = """
    <!DOCTYPE html>
    <html>
      <head>
      
      </head>
      <body>
        <h1>Attestation de déplacement dérogatoire</h2>
        <p> En application de l'article 3 du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid 19 dans le cadre de l'état d'urgence sanitaire <br></p>
        <p> Je soussigné(e)  </p>
        <p> Mme/M    $_prenom  $_nom </p>
        <p> Né(e) le </p>
        <p> A </p>
        <p> Demeurant : </p>
        <p> certifi que mon déplacement est lié au motif suivant (cocher la case) autorisé par l'article 3 du décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid 19 dans le cadre de l'état d'urgence sanitaire: <br></p>
        <div>
  <input type="checkbox"  name="value1"
         checked>
  <label for="value1"> "Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle, lorsqu\'ils sont indispensables à l\'exercice d’activités ne pouvant être organisées sous forme de télétravail ou déplacements professionnels ne pouvant être différés."<br></label>
</div> 
 <input type="checkbox"  name="value2"
         checked>
  <label for="value2"> "Déplacements pour effectuer des achats de fournitures nécessaires à l’activité professionnelle et des achats de première nécessité dans des établissements dont les activités demeurent autorisées (liste sur gouvernement.fr)"<br></label>
</div>
 <input type="checkbox"  name="value3"
         checked>
  <label for="value3"> "Consultations et soins ne pouvant être assurés à distance et ne pouvant être différés ; consultations et soins des patients atteints d'une affection de longue durée."<br></label>
</div>
 <input type="checkbox"  name="value4"
         checked>
  <label for="value4"> "Déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou la garde d’enfants." <br></label>
</div>
 <input type="checkbox"  name="value5"
         checked>
  <label for="value5"> "Déplacements brefs, dans la limite d'une heure quotidienne et dans un rayon maximal d'un kilomètre autour du domicile, liés soit à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective et de toute proximité avec d'autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile, soit aux besoins des animaux de compagnie."<br></label>
</div>
 <input type="checkbox"  name="value6"
         checked>
  <label for="value6"> "Convocation judiciaire ou administrative."<br></label>
</div>
 <input type="checkbox"  name="value7"
         checked>
  <label for="value7"> "Participation à des missions d’intérêt général sur demande de l’autorité administrative."<br> </label>
</div>
        <p> Fait à: </p>
        <p> Le :  à :  </p>
        <p> (date et heure de sortie) </p>
        
        
      </body>
    </html>
    """;

    Directory appDocDir = await getApplicationDocumentsDirectory();
    var targetPath = appDocDir.path;
    var targetFileName = "Attestation";

    var generatedPdfFile = await FlutterHtmlToPdf.convertFromHtmlContent(
        htmlContent, targetPath, targetFileName);
    generatedPdfFilePath = generatedPdfFile.path;
  }

  @override

  Widget build(BuildContext context) {
    //Construction du fond d'écran
    final background = Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(title: Text('Attestation'),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            background,

            Container(
                color: Colors.white.withOpacity(0.5),
                width: 450.0,
                height: 5000.0,
                child: SingleChildScrollView(


                    child: Form(
                        key: _formKey,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Nom"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Veuillez saisir votre nom";
                                      }
                                      return null;
                                    },
                                    onSaved: (input) => _nom = input,

                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Prénom"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Veuillez saisir votre prénom';
                                      }
                                      return null;
                                    },
                                    onSaved: (input) => _prenom = input,

                                  )
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 50.0),
                                child:  DateTimeField(
                                  decoration: InputDecoration(
                                      labelText: "Date de naissance"
                                  ),
                                  format: format,
                                  onShowPicker: (context, currentValue) {
                                    return showDatePicker(
                                        context: context,
                                        firstDate: DateTime(1900),
                                        initialDate: currentValue ?? DateTime.now(),
                                        lastDate: DateTime(2100));
                                  },
                                ),
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Lieu de naissance"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Veuillez saisir votre lieu de naissance';
                                      }
                                      return null;
                                    },

                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Adresse"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Veuillez saisir votre adresse';
                                      }
                                      return null;
                                    },

                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    decoration: InputDecoration(
                                        labelText: "Ville"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Veuillez saisir votre ville';
                                      }
                                      return null;
                                    },

                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                        labelText: "Code postal"
                                    ),
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Veuillez saisir votre code postal';
                                      }
                                      return null;
                                    },

                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: Text( 'Choisissez un motif de sortie',
                                    style: TextStyle(fontSize: 20.0,
                                    ),
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Déplacements entre le domicile et le lieu d’exercice de l’activité professionnelle, lorsqu\'ils sont indispensables à l\'exercice d’activités ne pouvant être organisées sous forme de télétravail ou déplacements professionnels ne pouvant être différés.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value1,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value1 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Déplacements pour effectuer des achats de fournitures nécessaires à l’activité professionnelle et des achats de première nécessité dans des établissements dont les activités demeurent autorisées (liste sur gouvernement.fr).",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value2,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value2 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Consultations et soins ne pouvant être assurés à distance et ne pouvant être différés ; consultations et soins des patients atteints d'une affection de longue durée.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value3,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value3 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Déplacements pour motif familial impérieux, pour l’assistance aux personnes vulnérables ou la garde d’enfants.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value4,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value4 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Déplacements brefs, dans la limite d'une heure quotidienne et dans un rayon maximal d'un kilomètre autour du domicile, liés soit à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective et de toute proximité avec d'autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile, soit aux besoins des animaux de compagnie.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value5,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value5 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Convocation judiciaire ou administrative.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value6,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value6 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),
                                  child: CheckboxListTile(
                                    title: Text("Participation à des missions d’intérêt général sur demande de l’autorité administrative.",
                                      textAlign: TextAlign.justify,
                                      maxLines: null,
                                      style: TextStyle(fontSize: 15.0,
                                      ),
                                    ),
                                    activeColor: Colors.cyan,
                                    value: _value7,
                                    onChanged: (bool newValue) {
                                      setState(() {
                                        _value7 = newValue;
                                      });
                                    },
                                  )
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 50.0),
                                child:  DateTimeField(
                                  decoration: InputDecoration(
                                      labelText: "Date de sortie"
                                  ),
                                  format: format,
                                  onShowPicker: (context, currentValue) {
                                    return showDatePicker(
                                        context: context,
                                        firstDate: DateTime(1900),
                                        initialDate: currentValue ?? DateTime.now(),
                                        lastDate: DateTime(2100));
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 50.0),
                                child:  DateTimeField(
                                  decoration: InputDecoration(
                                      labelText: "Heure de sortie"
                                  ),
                                  format: format2,
                                  onShowPicker: (context, currentValue) async {
                                    final time = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                                    );
                                    return DateTimeField.convert(time);
                                  },
                                ),
                              ),
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 50.0),

                                  child: MaterialButton(
                                    height: 40.0,
                                    minWidth: 1000.0,
                                    color: Colors.cyan,
                                    textColor: Colors.white,
                                    disabledColor: Colors.cyan,
                                    disabledTextColor: Colors.white,
                                    padding: EdgeInsets.all(8.0),
                                    splashColor: Colors.blueAccent,

                                    onPressed: Generator,


                                    child: Text('Générer mon attestation'),
                                  )
                              ),
                            ]
                        )
                    )
                )

            )
          ]
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ), child: null,
            ),
            ListTile(
              title: Text('Accueil',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Homepage()));
              },
            ),

            ListTile(
              title: Text('Les gestes barrières',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gestes()));
              },
            ),
            ListTile(
              title: Text('Les symptômes',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Symptomes()));
              },

            ),
            ListTile(
              title: Text('Attestation',
                style: TextStyle(fontSize: 20.0),
              ),

              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PDF()));
              },
            ),
            ListTile(
              title: Text('Questions',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionsPage()));
              },
            ),
            ListTile(
              title: Text('Posez nous vos question',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Formulaire()));
              },

            ),
          ],
        ),
      ),
    );
  }

  void Generator() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PDFViewerScaffold(
                appBar: AppBar(title: Text("Mon Attestation")),
                path: generatedPdfFilePath)
        ),
      );

    }
  }
}