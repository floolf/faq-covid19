import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Formulaire.dart';
import 'Homepage.dart';
import 'PDFGenerator.dart';
import 'Questions.dart';
import 'Symptomes.dart';

class Gestes extends StatefulWidget {

  @override
  _GestesState createState() => _GestesState();
}

class _GestesState extends State<Gestes> {

  @override
  Widget build(BuildContext context) {
    //Construction du fond d'écran
    final background = Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
    );

    return Scaffold(
        appBar: AppBar(title: Text("Les gestes barrières"),
    backgroundColor: Colors.blue,
    centerTitle: true,
    ),
        body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              background,
              Image(image: AssetImage('assets/images/gestes.png')
              ),
            ]
        ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blue,
              ), child: null,
            ),
            ListTile(
              title: Text('Accueil',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Homepage()));
              },
            ),

            ListTile(
              title: Text('Les gestes barrières',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gestes()));
              },
            ),
            ListTile(
              title: Text('Les symptômes',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Symptomes()));
              },

            ),
            ListTile(
              title: Text('Attestation',
                style: TextStyle(fontSize: 20.0),
              ),

              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PDF()));
              },
            ),
            ListTile(
              title: Text('Questions',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionsPage()));
              },
            ),
            ListTile(
              title: Text('Posez nous vos question',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Formulaire()));
              },

            ),
          ],
        ),
      ),
    );
}
}