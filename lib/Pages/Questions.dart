import 'package:faqcovid19/Pages/Formulaire.dart';
import 'package:faqcovid19/Pages/Homepage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Gestes.dart';
import 'PDFGenerator.dart';
import 'Symptomes.dart';

class QuestionsPage extends StatefulWidget {

  @override
  _QuestionsPageState createState() => _QuestionsPageState();
}

class _QuestionsPageState extends State<QuestionsPage> {

  @override
  Widget build(BuildContext context) {

    final background = Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
    );

    return Scaffold(
        appBar: AppBar(title: Text("Réponses à vos questions"),
        backgroundColor: Colors.blue,
        centerTitle: true,
        ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          background,
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  color: Colors.white.withOpacity(0.5),
                width: 390.0,
                height: 1000.0,
                  child:  ListView(
                    children: <Widget>[
                      ExpansionTile(
                        initiallyExpanded: false,
                        backgroundColor: Colors.white,
                            title: Text(
                            "Qu'est ce que le coronavirus?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                            ),
                          children: <Widget>[
                           Container(
                             child : Padding(
                               padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                child: Text(
                                " Les coronavirus sont une famille de virus, qui provoquent des maladies allant d’un simple rhume(certains virus saisonniers sont des coronavirus) à des pathologies plus sévères comme le MERS-CoV ou le SRAS.\n Le virus identifié en janvier 2020 en Chine est un nouveau coronavirus, nommé SARS-CoV-2. La maladie provoquée par ce coronavirus a été nommée COVID-19 par l’Organisation mondiale de la Santé - OMS. Depuis le 11 mars 2020, l’OMS qualifie la situation mondiale du COVID-19 de pandémie ; c’est-à-dire que l’épidémie est désormais mondiale. ",
                                textAlign: TextAlign.justify,
                                maxLines: null,
                                style: TextStyle(fontSize: 15.0,
                                ),
                          ),

                        )
                           )
                          ]
                  ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "D'où vient le coronavirus COVID 19?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    " Les premières personnes à avoir contracté le virus s’étaient rendues au marché de Wuhan dans la Province de Hubei en Chine. La maladie semblerait donc venir d’un animal (zoonose) mais l’origine n’a pas été confirmée. ",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Quels sont les symptômes du coronavirus COVID-19 ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    " Les symptômes principaux sont la fièvre ou la sensation de fièvre et la toux. \n La perte brutale de l’odorat, sans obstruction nasale et disparition totale du goût sont également des symptômes qui ont été observés chez les malades.\n Chez les personnes développant des formes plus graves, on retrouve des difficultés respiratoires, pouvant mener jusqu’à une hospitalisation en réanimation et au décès. ",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Quel est le délai d’incubation de la maladie ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Le délai d’incubation est la période entre la contamination et l’apparition des premiers symptômes. Le délai d’incubation du coronavirus COVID-19 est de 3 à 5 jours en général, il peut toutefois s’étendre jusqu’à 14 jours. Pendant cette période, le sujet peut être contagieux : il peut être porteur du virus avant l’apparition des symptômes ou à l’apparition de signaux faibles. " ,
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Le virus a-t-il muté ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                  "À ce jour il n’existe pas d’arguments scientifiques suffisamment robustes en cette faveur. Les travaux de recherche se poursuivent aujourd’hui pour mieux connaître le virus. \n Il convient de rappeler que la grande majorité des mutations des virus sont neutres, et le reste plus souvent bénéfiques pour l’Homme que l’inverse. En effet, dans la majorité des épidémies, les virus évoluent vers moins de dangerosité mais plus de diffusion.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Après avoir guéri du coronavirus, est-on immunisé ou est-il possible de tomber malade une deuxième fois ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Après avoir rencontré un virus, notre organisme développe des défenses immunitaires appelées anticorps, lui permettant de se défendre contre ce virus. Bien que nous soyons encore à un stade précoce pour se prononcer sur cette question, de l’avis des scientifiques les premières données semblent rassurantes, car ce jour, aucun cas réellement confirmé de re-contamination ne semble avoir eu lieu.",
                                   textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Comment se transmet le coronavirus COVID-19 ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "La maladie se transmet par les gouttelettes (sécrétions projetées invisibles lors d’une discussion, d’éternuements ou de la toux). On considère donc qu’un contact étroit avec une personne malade est nécessaire pour transmettre la maladie : même lieu de vie, contact direct à moins d’un mètre lors d’une discussion, d’une toux, d’un éternuement ou en l’absence de mesures de protection. \n Un des autres vecteurs privilégiés de la transmission du virus est le contact des mains non lavées souillées par des gouttelettes.\n C’est donc pourquoi les gestes barrières et les mesures de distanciation sociale sont indispensables pour se protéger de la maladie.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Peut-on être en contact d’un malade sans être contaminé ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Le risque est toujours présent, et plus le contact est long et rapproché, plus le risque de contamination augmente (plus de 15 minutes, à moins d’un mètre). C’est la raison pour laquelle la distanciation sociale et les mesures barrières doivent être appliquées. Il est donc possible de vivre avec un cas COVID-19 à domicile si l’on respecte scrupuleusement les gestes barrières.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Le virus circule-t-il dans l’air ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Non, il ne peut pas vivre dans l’air tout seul. Le coronavirus responsable du COVID-19 se transmet par les gouttelettes, qui sont les sécrétions respiratoires qu’on émet quand on tousse, qu’on éternue ou qu’on parle.\n Le virus est transporté par les gouttelettes, il ne circule pas dans l’air tout seul, mais peut atteindre une personne à proximité (<1 mètre) ou se fixer sur une surface souillée par les gouttelettes, comme les mains ou les mouchoirs. C’est pour cela qu’il est important de respecter les gestes barrières et les mesures de distanciation sociale.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Combien de temps le COVID-19 peut-il vivre sur une surface ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Dans des conditions propices à sa survie, le virus pourrait survivre, sous forme de traces, plusieurs jours sur une surface. Toutefois, ce n’est pas parce qu’un peu de virus survit que cela est suffisant pour contaminer une personne qui toucherait cette surface. En effet, au bout de quelques heures, la grande majorité du virus meurt et n’est probablement plus contagieux. Pour rappel, la grande transmissibilité du coronavirus COVID-19 n’est pas liée à sa survie sur les surfaces, mais à sa transmission quand on tousse, qu’on éternue , qu’on discute ou par les gouttelettes expulsées et transmises par les mains. C’est pour cela qu’il est important de respecter les gestes barrières et les mesures de distanciation sociale.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Les moustiques peuvent-ils transmettre le virus ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Non il n’existe aucune preuve de transmission du virus à travers les moustiques, ou tout autre animal d’ailleurs. Le coronavirus COVID-19 se transmet entre humains, via les gouttelettes respiratoires.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Peut-on attraper la maladie par l’eau ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "À ce jour, il n’a pas été rapporté de contamination par l’eau. Cette maladie est à transmission interhumaine par la voie des gouttelettes (toux, éternuements, mains souillées par les gouttelettes). La source originelle du virus n’est pas encore identifiée mais semble d’origine animale.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Existe-t-il des risques liés aux animaux domestiques (d’élevage et familiers) ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Il n’existe aucune preuve que les animaux domestiques jouent un rôle dans la propagation coronavirus COVID-19. De plus, le passage du coronavirus COVID-19 de l’être humain vers une autre espèce animale semble peu probable.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                      ExpansionTile(
                          initiallyExpanded: false,
                          backgroundColor: Colors.white,
                          title: Text(
                            "Existe-t-il des risques liés aux aliments ?",
                            style: TextStyle(fontSize: 17.0,
                            ),
                          ),
                          children: <Widget>[
                            Container(
                                child : Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                                  child: Text(
                                    "Au vu des informations disponibles, le passage du coronavirus COVID-19 de l’être humain vers une autre espèce animale semble actuellement peu probable, et la possible contamination des denrées alimentaires d’origine animale (DAOA) à partir d’un animal infecté par le COVID-19 est exclue.\n Les aliments crus ou peu cuits ne présentent pas de risques de transmission d’infection particuliers, dès lors que les bonnes règles d’hygiène habituelles sont respectées lors de la manipulation et de la préparation des denrées alimentaires.",
                                    textAlign: TextAlign.justify,
                                    maxLines: null,
                                    style: TextStyle(fontSize: 15.0,
                                    ),
                                  ),

                                )
                            )
                          ]
                      ),
                    ]
                )
              )
            ]
          )
        ]
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
        decoration: BoxDecoration(
          color: Colors.blue,
        ),
              ),
            ListTile(
              title: Text('Accueil',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Homepage()));
              },
            ),

            ListTile(
              title: Text('Les gestes barrières',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Gestes()));
              },
            ),
            ListTile(
                title: Text('Les symptômes',
                  style: TextStyle(fontSize: 20.0),
                ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Symptomes()));
              },

            ),
            ListTile(
                title: Text('Attestation',
                  style: TextStyle(fontSize: 20.0),
                ),

              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PDF()));
              },
            ),
            ListTile(
              title: Text('Questions',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => QuestionsPage()));
              },

            ),
            ListTile(
              title: Text('Posez nous vos question',
                style: TextStyle(fontSize: 20.0),
              ),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Formulaire()));
              },

            ),
          ],
        ),
      ),
    );
  }
}
