import 'package:flutter/material.dart';
import 'package:faqcovid19/Pages/Homepage.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid Inc.',
      theme: ThemeData(
          fontFamily: 'IM Fell French Canon SC'),
      home: Homepage(),
    );
  }
}

